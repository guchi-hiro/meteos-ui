/*
 * Copyright 2015 Hewlett Packard Enterprise Development Company LP
 * (c) Copyright 2015 ThoughtWorks Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular
    .module('horizon.framework.widgets')
    .controller('OverviewController', OverviewController)
    .directive('meteosTopology', meteosTopology);

  function meteosTopology() {
    var directive = {
      link: link,
      replace: true,
      restrict: 'E'
    };

    return directive;

    function link(scope, element) {

      var w = 800;
      var h = 500;
      var nodes = [ {id:0, label:"A"},
                    {id:1, label:"B"},
                    {id:2, label:"C"},
                    {id:3, label:"D"}
                  ];
      var links = [ {source:0, target:1},
                    {source:0, target:2},
                    {source:1, target:3},
                    {source:1, target:3}
                  ];
      var force = d3.layout.force()
                   .nodes(nodes)
                   .links(links)
                   .size([w, h])
                   .linkStrength(0.1)
                   .friction(0.9)
                   .distance(200)
                   .charge(-30)
                   .gravity(0.1)
                   .theta(0.8)
                   .alpha(0.1)
                   .start();

      var svg = d3.select('#meteosTopologyCanvas').append("svg")
          .attr("width", width)
          .attr("height", height);

      var link = svg.selectAll("line")
                    .data(links)
                    .enter()
                    .append("line")
                    .style({"stroke": "#ccc",
                            "stroke-width": 1});

      var node = svg.selectAll("circle")
                    .data(nodes)
                    .enter()
                    .append("circle")
                    .attr({r: 20,
                           opacity: 0.5})
                    .style({"fill": "red"})
                    .call(force.drag);

      var label = svg.selectAll('text')
                      .data(nodes)
                      .enter()
                      .append('text')
                      .attr({"text-anchor":"middle",
                             "fill":"white"})
                      .style({"font-size":11})
                      .text(function(d) { return d.label; });

      force.on("tick", function() {
        link.attr({x1: function(d) { return d.source.x; },
                   y1: function(d) { return d.source.y; },
                   x2: function(d) { return d.target.x; },
                   y2: function(d) { return d.target.y; }});
        node.attr({cx: function(d) { return d.x; },
                   cy: function(d) { return d.y; }});
        label.attr({x: function(d) { return d.x; },
                    y: function(d) { return d.y; }});
      });

    }
  }


  OverviewController.$inject = [
    '$scope',
    'horizon.framework.widgets.charts.quotaChartDefaults'
  ];

  function OverviewController($scope,
    quotaChartDefaults
  ) {

    var ctrl = this;

    ctrl.instanceStats = {
      title: gettext('Total Instances'),
      maxLimit: 10,
      label: '100%',
      data: [
        {
          label: quotaChartDefaults.usageLabel,
          value: 0,
          colorClass: quotaChartDefaults.usageColorClass
        },
        {
          label: quotaChartDefaults.addedLabel,
          value: 1,
          colorClass: quotaChartDefaults.addedColorClass
        },
        {
          label: quotaChartDefaults.remainingLabel,
          value: 9,
          colorClass: quotaChartDefaults.remainingColorClass
        }
      ]
    };
  }
})();
